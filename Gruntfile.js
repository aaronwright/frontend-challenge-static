module.exports = function(grunt) {
    const sass = require('node-sass');
      
    grunt.initConfig({
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            build: {
                files: [{
                    src: './src/sass/main.scss',
                    dest: './src/css/main.css'
                }]
            }
        }
    });
     
    grunt.loadNpmTasks('grunt-sass');
    
    }